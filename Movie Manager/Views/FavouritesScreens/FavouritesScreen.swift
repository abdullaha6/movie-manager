//
//  FavouritesScreen.swift
//  Movie Manager
//
//  Created by Mohammed Ajmal on 2022-08-10.
//

import UIKit
import RxSwift

class FavouritesScreen: UIViewController {

    // Create the table view
    private var favouriteMoviesTableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = UIColor(named: "bgColor")
        tv.register(FavouriteMoviesTableViewCell.self, forCellReuseIdentifier: "cell")
        return tv
    }()
    
    // Create an instance of the Movie view model
    private var movieViewModel = FavouriteMovieViewModel()
    
    private var bag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(named: "bgColor")
        title = "Favourites"
        
        setupTableView()
    }
    
    func setupTableView() {
        
        favouriteMoviesTableView.frame = view.bounds
        favouriteMoviesTableView.rx.setDelegate(self)
        view.addSubview(favouriteMoviesTableView)
        bindTableData()
    }
    
    func bindTableData() {
        
        let detailScreen = DetailScreen()
        
        // Bind items to table
        movieViewModel.items.bind(to: favouriteMoviesTableView.rx.items(cellIdentifier: "cell", cellType: FavouriteMoviesTableViewCell.self)
        ) { row, model, cell in
            
            cell.tableViewCellData = model
            cell.backgroundColor = UIColor(named: "bgColor")
            
        }.disposed(by: bag)
        
        // Bind a model selected handler
        favouriteMoviesTableView.rx.modelSelected(Favourites.self).bind { favMovie in
            
            // Pass the selected movie object
            detailScreen.selectedFavMovie = favMovie
            detailScreen.isMovieScreen = false
            self.navigationController?.pushViewController(detailScreen, animated: true)
            
        }.disposed(by: bag)
        
        // Fetch items
        movieViewModel.retrieveFavouriteMovies()
    }
}

extension FavouritesScreen: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
