//
//  FavouriteMoviesTableViewCell.swift
//  Movie Manager
//
//  Created by Mohammed Ajmal on 2022-08-13.
//

import UIKit
import Alamofire
import CoreData

class FavouriteMoviesTableViewCell: UITableViewCell {
    
    var tableViewCellData: Favourites? {
        didSet {
            guard let cellData = tableViewCellData else { return }
            
            // Update the cell elements
            retreiveImage(id: cellData.trackId!)
            checkIfFavourite(id: cellData.trackId!)
            trackNameLabel.text = cellData.trackName
            priceLabel.text = "A$ \(cellData.trackPrice)"
            genreLabel.text = cellData.trackGenre
        }
    }
    
    // Create the image view for the track images
    let trackImageView: UIImageView = {

        let imageView = UIImageView()
        imageView.backgroundColor = .green
        imageView.contentMode = .scaleAspectFit
        imageView.sizeToFit()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    // Create the favourite button
    let favouriteButton: UIButton = {
        
        let button = UIButton()
        button.setImage(UIImage(systemName: "heart")!
            .withTintColor(UIColor(named: "themeColor")!, renderingMode: .alwaysOriginal), for: .normal)
        button.setImage(UIImage(systemName: "heart.fill")!
            .withTintColor(UIColor(named: "themeColor")!, renderingMode: .alwaysOriginal), for: .selected)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    // Create the title for the track
    let trackNameLabel: UILabel = {

        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIFont(name: "Avenir-Heavy", size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // Create the price text label
    let priceLabel: UILabel = {
        
        let label = UILabel()
        label.textColor = UIColor(named: "themeColor")
        label.textAlignment = .left
        label.font = UIFont(name: "Futura", size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // Create the genre text label
    let genreLabel: UILabel = {
        
        let label = UILabel()
        label.textColor = .gray
        label.textAlignment = .left
        label.font = UIFont(name: "Futura", size: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func retreiveImage(id: String) {
        if  let result = try? context.fetch(Favourites.fetchRequest()) {
            for object in result {
                
                if object.trackId == id {
                    trackImageView.image = UIImage(data: object.trackImage!)
                }
            }
        }
    }
    
    // Create a reference to the managed object
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // Create a function to check if movie is a favourite
    func checkIfFavourite(id: String) {

        if  let result = try? context.fetch(Favourites.fetchRequest()) {
            for object in result {
                
                if object.trackId == id {
                    favouriteButton.isSelected = true
                }
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        // Add the elements to the cell
        contentView.addSubview(trackImageView)
        contentView.addSubview(favouriteButton)
        contentView.addSubview(trackNameLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(genreLabel)

        // Add the constraints
        trackImageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        trackImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        trackImageView.widthAnchor.constraint(equalToConstant: 100.5).isActive = true
        trackImageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        favouriteButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        favouriteButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        favouriteButton.widthAnchor.constraint(equalToConstant: 20).isActive = true

        trackNameLabel.topAnchor.constraint(equalTo: trackImageView.topAnchor, constant: 10).isActive = true
        trackNameLabel.leadingAnchor.constraint(equalTo: trackImageView.trailingAnchor, constant: 10).isActive = true
        trackNameLabel.trailingAnchor.constraint(equalTo: favouriteButton.leadingAnchor, constant: -5).isActive = true
        
        priceLabel.topAnchor.constraint(equalTo: trackNameLabel.bottomAnchor, constant: 20).isActive = true
        priceLabel.leadingAnchor.constraint(equalTo: trackNameLabel.leadingAnchor).isActive = true
        
        genreLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 10).isActive = true
        genreLabel.leadingAnchor.constraint(equalTo: trackNameLabel.leadingAnchor).isActive = true
        genreLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
