//
//  MoviesScreen.swift
//  Movie Manager
//
//  Created by Mohammed Ajmal on 2022-08-10.
//

import UIKit
import RxSwift

class MoviesScreen: UIViewController {
    
    // Create the table view
    private var moviesTableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = UIColor(named: "bgColor")
        tv.register(MoviesTableViewCell.self, forCellReuseIdentifier: "cell")
        return tv
    }()
    
    // Create an instance of the Movie view model
    private var movieViewModel = MovieViewModel()
    
    private var bag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(named: "bgColor")
        title = "Movies"
        
        setupTableView()
    }
    
    func setupTableView() {
        
        moviesTableView.frame = view.bounds
        moviesTableView.rx.setDelegate(self)
        view.addSubview(moviesTableView)
        bindTableData()
    }
    
    func bindTableData() {
        
        let detailScreen = DetailScreen()
        
        // Bind items to table
        movieViewModel.items.bind(to: moviesTableView.rx.items(cellIdentifier: "cell", cellType: MoviesTableViewCell.self)
        ) { row, model, cell in
            
            cell.tableViewCellData = model
            cell.backgroundColor = UIColor(named: "bgColor")
            
        }.disposed(by: bag)
        
        // Bind a model selected handler
        moviesTableView.rx.modelSelected(Movie.self).bind { movie in
            
            // Pass the selected movie object
            detailScreen.selectedMovie = movie
            detailScreen.isMovieScreen = true
            self.navigationController?.pushViewController(detailScreen, animated: true)
            
        }.disposed(by: bag)
        
        // Fetch items
        movieViewModel.fetchMovies()
    }
}


extension MoviesScreen: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

