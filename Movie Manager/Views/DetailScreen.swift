//
//  DetailScreen.swift
//  Movie Manager
//
//  Created by Mohammed Ajmal on 2022-08-12.
//

import UIKit
import Alamofire
import CoreData

class DetailScreen: UIViewController {
    
    // Create a bool to check if the user is from movie screen
    var isMovieScreen: Bool = true
    
    // Create the variable to store the selected movie
    var selectedMovie: Movie = Movie(trackId: "", trackName: "", isFavourite: false, price: 0, genre: "", artworkUrl: "", description: "")
    
    var selectedFavMovie: Favourites?
    
    // Create the navigation bar button
    let addToFavouritesButton: UIButton = {
        
        let button = UIButton()
        button.setImage(UIImage(systemName: "heart")!
            .withTintColor(UIColor(named: "themeColor")!, renderingMode: .alwaysOriginal), for: .normal)
        button.setImage(UIImage(systemName: "heart.fill")!
            .withTintColor(UIColor(named: "themeColor")!, renderingMode: .alwaysOriginal), for: .selected)
        return button
    }()
    
    // Create a scroll view
    let scrollView: UIScrollView = {
        
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    // Create a container view for the shadow of the image view
    let containerView: UIView = {
        
        let view = UIView()
        view.layer.cornerRadius = 10
        view.backgroundColor = UIColor(named: "bgColor")
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 5.0)
        view.layer.shadowRadius = 25.0
        view.layer.shadowOpacity = 0.9
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // Create an image view for the movie image
    let trackImageView: UIImageView = {

        let imageView = UIImageView()
        imageView.image = UIImage(named: "movie")
        imageView.backgroundColor = .green
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        imageView.sizeToFit()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    // Create the title for the track
    let trackNameLabel: UILabel = {

        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont(name: "Avenir-Heavy", size: 23)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // Create the price text label
    let priceLabel: UILabel = {
        
        let label = UILabel()
        label.textColor = UIColor(named: "themeColor")
        label.textAlignment = .center
        label.font = UIFont(name: "Avenir-Heavy", size: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // Create a text for the description text
    let descriptionTextView: UITextView = {
        
        let tv = UITextView()
        tv.textColor = .gray
        tv.textAlignment = .justified
        tv.backgroundColor = UIColor(named: "bgColor")
        tv.isScrollEnabled = false
        tv.font = UIFont(name: "Avenir-Medium", size: 18)
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    // Create the genre text label
    let genreLabel: UILabel = {
        
        let label = UILabel()
        label.textColor = UIColor(named: "themeColor")
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont(name: "Avenir-Light", size: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if  let result = try?  context.fetch(Favourites.fetchRequest()){
//            for object in result {
//                print(object.trackName)
//                context.delete(object)
//            }
//        }
        // Save the data
//        do {
//            try context.save()
//            print("deleted")
//
//        } catch {
//            print("Failed to delete from core data with error: \(error)")
//        }
        
        setupScreen()
        setupBarButtonItems()
        addScrollViewConstraints()
        addContainerViewConstraints()
        addTrackImageViewConstraints()
        addTrackNameLabelConstraints()
        addPriceLabelConstraints()
        addDescriptionTextViewConstraints()
        addGenreLabelConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        if isMovieScreen {
            assignMoviePassedData()
        } else {
            assignFavouritePassedData()
        }
        
        checkIfMovieIsFavourite()
    }
    
    // MARK: Setups
    func setupScreen() {
        
        title = "Details"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        view.backgroundColor = UIColor(named: "bgColor")
    }
    
    func setupBarButtonItems() {
        
        addToFavouritesButton.addTarget(self, action: #selector(favouriteButtonTapped), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addToFavouritesButton)
    }
    
    // Create a function to assign the passed data
    func assignMoviePassedData() {
        
        // Download the image
        AF.request(selectedMovie.artworkUrl).response { response in
            DispatchQueue.main.async { // Display using main thread
                
                if let image = UIImage(data: response.data!) {
                    self.trackImageView.image = image
                }
            }
        }
        
        trackNameLabel.text = selectedMovie.trackName
        priceLabel.text = "A$ \(selectedMovie.price)"
        descriptionTextView.text = selectedMovie.description
        genreLabel.text = selectedMovie.genre
    }
    
    func assignFavouritePassedData() {
        
        trackImageView.image = UIImage(data: selectedFavMovie!.trackImage!)
        trackNameLabel.text = selectedFavMovie!.trackName
        priceLabel.text = "A$ \(selectedFavMovie!.trackPrice)"
        descriptionTextView.text = selectedFavMovie!.trackDescription
        genreLabel.text = selectedFavMovie!.trackGenre
    }
    
    // MARK: Constraints
    // Add scroll view constraints
    func addScrollViewConstraints() {
        
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    // Add constraints to the container view
    func addContainerViewConstraints() {
        
        scrollView.addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 20).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        containerView.widthAnchor.constraint(equalToConstant: 134).isActive = true
        containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    // Add constraints to the track image view
    func addTrackImageViewConstraints() {
        
        scrollView.addSubview(trackImageView)
        trackImageView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 20).isActive = true
        trackImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        trackImageView.widthAnchor.constraint(equalToConstant: 134).isActive = true
        trackImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    // Add constraints to the track name label
    func addTrackNameLabelConstraints() {
        
        scrollView.addSubview(trackNameLabel)
        trackNameLabel.topAnchor.constraint(equalTo: trackImageView.bottomAnchor, constant: 25).isActive = true
        trackNameLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        trackNameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
    }
    
    // Add constraints to the price label
    func addPriceLabelConstraints() {
        
        scrollView.addSubview(priceLabel)
        priceLabel.topAnchor.constraint(equalTo: trackNameLabel.bottomAnchor, constant: 15).isActive = true
        priceLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    // Add constraints to the description text view
    func addDescriptionTextViewConstraints() {
        
        scrollView.addSubview(descriptionTextView)
        descriptionTextView.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 15).isActive = true
        descriptionTextView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        descriptionTextView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
    }
    
    // Add constraints to the genre label
    func addGenreLabelConstraints() {
        
        scrollView.addSubview(genreLabel)
        genreLabel.topAnchor.constraint(equalTo: descriptionTextView.bottomAnchor, constant: 15).isActive = true
        genreLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        genreLabel.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20).isActive = true
    }
    
    // Create a reference to the managed object
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // MARK: Selectors
    @objc func favouriteButtonTapped() {
        
        // To select or unselect button
        addToFavouritesButton.isSelected = !addToFavouritesButton.isSelected
        
        if addToFavouritesButton.isSelected { // Add to favourites
            addToFavourites()

        } else { // Remove from favourites
            removeFromFavourites()
        }
    }
    
    // Create a function to check if the selected movie if favourite or not
    func checkIfMovieIsFavourite() {
        
        if  let result = try? context.fetch(Favourites.fetchRequest()) {
            for object in result {
                
                if isMovieScreen {
                    if object.trackId! == selectedMovie.trackId {
                        addToFavouritesButton.isSelected = true
                        break // Exit the loop if favourite found
                        
                    } else {
                        addToFavouritesButton.isSelected = false
                    }
                } else {
                    addToFavouritesButton.isSelected = true
                }
            }
        }
    }
    
    // Save the selected movie to database
    func addToFavourites() {
        
        // Convert image to data
        let jpegImageData = trackImageView.image!.jpegData(compressionQuality: 1.0)
        
        // Create a new favourite movie object
        let newFavouriteMovie = Favourites(context: self.context)
        newFavouriteMovie.trackId = selectedMovie.trackId
        newFavouriteMovie.trackImage = jpegImageData
        newFavouriteMovie.trackName = selectedMovie.trackName
        newFavouriteMovie.trackPrice = selectedMovie.price
        newFavouriteMovie.trackDescription = selectedMovie.description
        newFavouriteMovie.trackGenre = selectedMovie.genre
        
        // Save the data
        do {
            try context.save()
            print("saved")
            
        } catch {
            print("Failed to save to core data with error: \(error)")
        }
    }
    
    // Remove the selected movie from database
    func removeFromFavourites() {
        
        if  let result = try?  context.fetch(Favourites.fetchRequest()){
            for object in result {
                
                if object.trackId == selectedMovie.trackId {
                    context.delete(object)
                }
            }
        }
        // Save the data
        do {
            try context.save()
            print("deleted")

        } catch {
            print("Failed to delete from core data with error: \(error)")
        }
    }
}
