//
//  TabBarController.swift
//  Movie Manager
//
//  Created by Mohammed Ajmal on 2022-08-10.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTabBarItems()
    }
    
    // Create a function to setup the tab bar items
    func setupTabBarItems() {
        
        // Setup Nav Bar
        let vc1 = UINavigationController(rootViewController: MoviesScreen())
        let vc2 = UINavigationController(rootViewController: FavouritesScreen())
        
        vc1.navigationBar.prefersLargeTitles = true
        vc2.navigationBar.prefersLargeTitles = true

        // Setup Tab Bar
        vc1.title = "Movies"
        vc1.tabBarItem.image = UIImage(systemName: "film")
        vc1.tabBarItem.selectedImage = UIImage(systemName: "film.fill")
        vc2.title = "Favourites"
        vc2.tabBarItem.image = UIImage(systemName: "heart")
        vc2.tabBarItem.selectedImage = UIImage(systemName: "heart.fill")

        self.setViewControllers([vc1, vc2], animated: false)
        
        tabBar.backgroundColor = UIColor(named: "bgColor")
        tabBar.tintColor = UIColor(named: "themeColor")
    }
}
