//
//  Favourites+CoreDataProperties.swift
//  
//
//  Created by Mohammed Ajmal on 2022-08-13.
//
//

import Foundation
import CoreData


extension Favourites {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Favourites> {
        return NSFetchRequest<Favourites>(entityName: "Favourites")
    }

    @NSManaged public var trackId: String?
    @NSManaged public var trackImage: Data?
    @NSManaged public var trackName: String?
    @NSManaged public var trackPrice: Double
    @NSManaged public var trackDescription: String?
    @NSManaged public var trackGenre: String?

}
