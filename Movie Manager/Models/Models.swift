//
//  MovieModel.swift
//  Movie Manager
//
//  Created by Mohammed Ajmal on 2022-08-11.
//

import UIKit

// Movie model
struct Movie {
    
    let trackId: String
    let trackName: String
    let isFavourite: Bool
    let price: Double
    let genre: String
    let artworkUrl: String
    let description: String
}
