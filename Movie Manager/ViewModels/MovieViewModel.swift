//
//  MovieViewModel.swift
//  Movie Manager
//
//  Created by Mohammed Ajmal on 2022-08-11.
//

import Foundation
import RxSwift
import RxCocoa
import Alamofire

// Choosing class instead of struct to avoid mutating functions
class MovieViewModel {
    
    var items = PublishSubject<[Movie]>()
    // Create an array to store the all the movies
    var movies: [Movie] = []
    
    // Fetch all movies
    func fetchMovies() {
        // Check if file exists
        checkIfFileExists()
    }
    
    // Create a function to check if the file already exists
    func checkIfFileExists() {
        
        // Check if the file already exists in the documents directory
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let fileDirectory = NSURL(fileURLWithPath: path)
        
        if let pathComponent = fileDirectory.appendingPathComponent("1.txt") {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            
            if fileManager.fileExists(atPath: filePath) {
                
                // Read and convert the data file to json
                readDataFile(pathComponent: pathComponent)
                
            } else { // Download the data file
                downloadDataFile(pathComponent: pathComponent)
            }
        }
    }
    
    // Create a function to read the file
    func readDataFile(pathComponent: URL) {
        
        do {
            let jsonString = try String(contentsOf: pathComponent, encoding: .utf8)
            
            // Convert jsonString to json format
            let jsonData = jsonString.data(using: .utf8)!
            
            // Decode the json
            if let moviesArray = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String: Any] {
                if let allMoviesArray = moviesArray["results"] as? [[String: Any]] {

                    for movie in allMoviesArray {

                        let trackId = String(movie["trackId"] as! Int)
                        let trackName = movie["trackName"] as! String
                        let price = movie["trackPrice"] as! Double
                        let genre = movie["primaryGenreName"] as! String
                        let artworkUrl = movie["artworkUrl100"] as! String
                        let description = movie["longDescription"] as! String
                        
                        let movieData = Movie(trackId: trackId, trackName: trackName, isFavourite: false, price: price, genre: genre, artworkUrl: artworkUrl, description: description)
                        movies.append(movieData)
                    }
                    
                    // All data retrieved, reload table view
                    items.onNext(movies)
                    items.onCompleted()
                }
            }
        }
        catch {
            print("Error reading file")
        }
    }
    
    // Create a function to download data file and store the file in documents directory
    func downloadDataFile(pathComponent: URL) {
        let url = "https://itunes.apple.com/search?term=star&country=au&media=movie&all"
        // Save the file to documents directory
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        
        AF.download(url, to: destination).response { response in
            
            if response.error == nil {
                print("success")
                
                // Read and convert the data file to json
                self.readDataFile(pathComponent: pathComponent)
            }
        }
    }
    
}

