//
//  FavouriteMovieViewModel.swift
//  Movie Manager
//
//  Created by Mohammed Ajmal on 2022-08-13.
//

import UIKit
import CoreData
import RxSwift
import RxCocoa

class FavouriteMovieViewModel {
    
    var items = PublishSubject<[Favourites]>()
    // Create an array to store the all the movies
    var movies: [Favourites] = []
    // Create a reference to the managed object
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // Create a function to retrieve favourite movies
    func retrieveFavouriteMovies() {
        
        if  let result = try? context.fetch(Favourites.fetchRequest()) {
            for object in result {
                
                movies.append(object)
            }
            
            // All data retrieved, reload table view
            items.onNext(movies)
            items.onCompleted()
        }
    }
}
