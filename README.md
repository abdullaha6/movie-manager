# Movie Manager



## About the application

Movie Manager is an application that displays movies from iTunes. It consists of 50 movies retrieved from the iTunes API (https://itunes.apple.com/search?term=star&country=au&media=movie&). Here's a list of all the movie related data that are being displayed on the application:
* Movie title
* Artwork
* Price
* Genre
* Description

The application also allows you to add movies you like to favourites.

## Running the application
To run this application, first clone this repo and install the necessary pods by running `pod install` in the terminal. Once, the pods are installed, open Movie Manager.xcworkspace and run the application.
To get the best out of the application, run it using an **iPhone 12 Pro Max** or **13 Pro Max simulator**.

## Persistence
All the favourite movies are saved in the Core Data database so that the user can view the favourites even when the application is offline.

## Architecture
The application follows the MVVM design pattern as it was the recommended design pattern for this application.

## Tech Stack
* Swift 5
* MVVM (Model-View-ViewModel)
* UIKit
* Cocoa Pods
* RxSwift
* Alamofire
* Core Data
* Git workflow
